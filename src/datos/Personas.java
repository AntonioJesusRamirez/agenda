package datos;

/**
 *
 * @author Antonio
 */

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Personas {

    // A la etiqueta que identifique el inicio y fin de cada item en el XML, se le va
    //  a llamar 'item' en lugar de 'itemlist'
    @XmlElement(name = "persona")
    private List<Persona> arrayListPersonas;

    public Personas() {
        arrayListPersonas = new ArrayList();
    }

    public List<Persona> getArrayListPersonas() {
        return arrayListPersonas;
    }

    public void setArrayListPersonas(List<Persona> arrayList) {
        this.arrayListPersonas = arrayList;
    }

}