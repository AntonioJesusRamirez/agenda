package datos;

import java.util.Date;
import javafx.scene.control.DatePicker;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Antonio
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Persona {
    @XmlElement
    private int id;
    @XmlElement
    private String nombre;
    @XmlElement
    private String apellidos;
    @XmlElement
    private String sexo;
    @XmlElement
    private String dni;
    @XmlElement
    private String direccion;
    @XmlElement
    private String cP;
    @XmlElement
    private String ciudad;
    @XmlElement
    private String telefonoFijo;
    @XmlElement
    private String telefonoMovil;
    @XmlElement
    private Date fechaNac;
    
    
    public Persona(){
        
    }
    
    public Persona(String nombre, String apellidos, String sexo, 
            String dni, String direccion, String cP, String ciudad, String telefonoFijo,
            String telefonoMovil, Date datePickerFecha){


        this.nombre = nombre;
        this.apellidos = apellidos;
        this.sexo = sexo;
        this.dni = dni;
        this.direccion = direccion;
        this.cP = cP;
        this.ciudad = ciudad;
        this.telefonoFijo = telefonoFijo;
        this.telefonoMovil = telefonoMovil;
        this.fechaNac = datePickerFecha;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCP() {
        return cP;
    }

    public void setcP(String cP) {
        this.cP = cP;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date datePickerFecha) {
        this.fechaNac = datePickerFecha;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
}
