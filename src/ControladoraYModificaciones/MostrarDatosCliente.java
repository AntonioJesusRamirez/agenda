package ControladoraYModificaciones;

import datos.Persona;
import datos.Personas;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Antonio
 */
public class MostrarDatosCliente {
    
 
    public static void main(String[] args) {
        
        try {
            // Crear una conexión con la URL del Servlet
            String strConnection = "http://localhost:8084/WebApplication1/servidorLocal";
            URL url = new URL(strConnection);
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            // La conexión se va a realizar para poder enviar y recibir información
            //   en formato XML
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "text/xml");
            // Se va a realizar una petición con el método GET
            conn.setRequestMethod("GET");
 
            // Ejecutar la conexión y obtener la respuesta
            InputStreamReader isr = new InputStreamReader(conn.getInputStream());
 
            // Procesar la respuesta (XML) y obtenerla como un objeto de tipo Personas
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Object response = jaxbUnmarshaller.unmarshal(isr);
            isr.close();
 
            // Convertir a la clase Personas el objeto obtenido en la respuesta
            Personas personas = (Personas)response;
            
            // Como ejemplo, se mostrará en la salida estándar alguno de los datos
            //  de los objetos contenidos en la lista que se encuentra en items
            for(Persona persona : personas.getArrayListPersonas()) {
                System.out.println(persona.getNombre() + " " + persona.getApellidos()
                        +  " " + persona.getDni());
            }
 
        } catch (JAXBException ex) {
            Logger.getLogger(MostrarDatosCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MostrarDatosCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
