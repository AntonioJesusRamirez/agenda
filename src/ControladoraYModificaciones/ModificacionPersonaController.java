package ControladoraYModificaciones;

import datos.Persona;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import agenda.DisenoPersona;
import agenda.LeerPersona;
import agenda.Agenda;
import conversor.GenerarArrayList;
import conversor.GenerarXML;
import datos.Personas;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.control.DatePicker;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;


/**
 * FXML Controller class
 *
 * @author Antonio
 */
public class ModificacionPersonaController implements Initializable {
    @FXML
    private TextField nombre;
    @FXML
    private TextField apellidos;
    @FXML
    private TextField sexo;
    @FXML
    private TextField dni;
    @FXML
    private TextField direccion;
    @FXML
    private TextField codigoPostal;
    @FXML
    private TextField ciudad;
    @FXML
    private TextField telefonoFijo;
    @FXML
    private TextField telefonoMovil;
    @FXML
    private DatePicker fechaNac;
    
    public static final byte PETICION_GET = 0;    // SELECT
    public static final byte PETICION_POST = 1;   // INSERT
    public static final byte PETICION_PUT = 2;    // UPDATE
    public static final byte PETICION_DELETE = 3; // DELETE
    private byte tipoPeticion;
    
    public static Persona persona = new Persona();
    public static TableView<Persona> tableView;
    public static  Personas personas  = new Personas();
    


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
    /**
     * Vuelve a la vista anterior.
     * @param event Es el click del ratón.
     */
    @FXML
    private void OnMouseClickedVolver(MouseEvent event) {
        
        Agenda.root.getChildren().remove(1);
    }
    
    /**
     * Edita o crea un nuevo registro.
     * @param event Es el click del ratón.
     */
    @FXML
    private void OnMouseClickedGuardar(MouseEvent event) {
        
        if(DisenoPersona.editarRegistro){
            try{
                persona.setNombre(nombre.getText());
                persona.setApellidos(apellidos.getText());
                persona.setSexo(sexo.getText());
                persona.setDni(dni.getText());
                persona.setDireccion(direccion.getText());
                persona.setcP(codigoPostal.getText());
                persona.setCiudad(ciudad.getText());
                persona.setTelefonoFijo(telefonoFijo.getText());
                persona.setTelefonoMovil(telefonoMovil.getText());
                persona.setFechaNac(datos.Fecha.getDateFromDatePicket(fechaNac));
                int indiceDeRegistro = tableView.getSelectionModel().getSelectedIndex();
                tableView.getItems().set(indiceDeRegistro, persona);
                tipoPeticion = PETICION_PUT;
                conexionServlet(persona, tipoPeticion);
            }catch(NullPointerException ex){
                Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            try{
                tableView.getItems().add(persona = new Persona(nombre.getText(),apellidos.getText(),
                        sexo.getText(),dni.getText(), direccion.getText(),
                        codigoPostal.getText(), ciudad.getText(),telefonoFijo.getText(),
                        telefonoMovil.getText(), datos.Fecha.getDateFromDatePicket(fechaNac)));
                tipoPeticion = PETICION_POST;
                conexionServlet(persona, tipoPeticion);
            }catch(NullPointerException ex){
                Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        DisenoPersona.editarRegistro = false;
        Agenda.root.getChildren().remove(1);
        
        //Guardar el arrayList en un fichero .XML
        Personas personas = new Personas();
        personas.getArrayListPersonas().add(persona);
        GenerarXML.generarXML(personas);
        
        //Guardar el XML en el arryList
        GenerarArrayList.generarArrayList();
        
    }
    
    /**
     * Selecciona los datos del registro seleccionado.
     * @param indiceDeRegistro Es el registro escojido.
     */
    public void setPersona(Persona persona){
        
        try{
            this.persona = persona;
            nombre.setText(persona.getNombre());
            apellidos.setText(persona.getApellidos());
            sexo.setText(persona.getSexo());
            dni.setText(persona.getDni());
            direccion.setText(persona.getDireccion());
            codigoPostal.setText(persona.getCP());
            ciudad.setText(persona.getCiudad());
            telefonoFijo.setText(persona.getTelefonoFijo());
            telefonoMovil.setText(persona.getTelefonoMovil());
            datos.Fecha.setDateInDatePicker(fechaNac,
                    datos.Fecha.getDateFromDatePicket(fechaNac));
            
        }catch(NullPointerException ex){
            Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

    /**
     * Cambia la vista de la pantalla.
     * @param tableView Aztualiza la nueva vista que mostrara.
     */
    public void setView(TableView tableView){

        this.tableView = tableView;
    }

    public static Personas conexionServlet(Persona persona, Byte tipoPeticion){
        
        try {
            String strConnection = "http://localhost:8084/WebApplication1/servidorLocal";
            URL url = new URL(strConnection);
            URLConnection uc = url.openConnection();
            HttpURLConnection conn = (HttpURLConnection) uc;
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "text/xml");
 
            
            
            // Convertir objeto personas a XML y preparar para enviar al servidor.
//            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
//            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            
            switch(tipoPeticion){
                case PETICION_GET:{
                    conn.setRequestMethod("GET");
                    
                    // Convertir objeto personas a XML y preparar para enviar al servidor.
                    JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
                    InputStreamReader isr = new InputStreamReader(conn.getInputStream());
                    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                    personas = (Personas) jaxbUnmarshaller.unmarshal(isr);
                    isr.close();
                    
                    return personas;
                }
                case PETICION_POST:{
                    conn.setRequestMethod("POST");
                    
                    //Añadir una persona.
                    // Aunque sólo se va a enviar un único objeto, se utilizará un objeto
                    //  de la clase Personas que almacenará dicho objeto. Se va a hacer
                    //  así para usar siempre la clase Personas al generar los XML
                    Personas personasNuevas = new Personas();
                    personasNuevas.getArrayListPersonas().add(persona);
                    Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE,
                            String.valueOf(personasNuevas.getArrayListPersonas().size()));
                    // Convertir objeto personas a XML y preparar para enviar al servidor.
                    JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
                    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                    jaxbMarshaller.marshal(personasNuevas, conn.getOutputStream());
                    // Ejecutar la conexión y obtener la respuesta
                    InputStreamReader isr = new InputStreamReader(conn.getInputStream());
                    
                    return personasNuevas;
                }
                case PETICION_PUT:{
                    conn.setRequestMethod("PUT");
                    
                    int indiceDeRegistro = tableView.getSelectionModel().getSelectedIndex();
                    Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.INFO,
                            persona.getNombre());
                    // Convertir objeto personas a XML y preparar para enviar al servidor.
                    JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
                    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                    jaxbMarshaller.marshal(personas, conn.getOutputStream());
                    personas.getArrayListPersonas().set(indiceDeRegistro, persona);
                    return personas;
                }
                case PETICION_DELETE:{
                    conn.setRequestMethod("DELETE");
//                    int indiceDeRegistro = tableView.getSelectionModel().getSelectedIndex();
                    int indiceDeRegistro = persona.getId()+1;
                    Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.INFO,
                            persona.getNombre());
                    // Convertir objeto personas a XML y preparar para enviar al servidor.
                    JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
                    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                    jaxbMarshaller.marshal(personas, conn.getOutputStream());
                    personas.getArrayListPersonas().set(indiceDeRegistro, persona);
                    
                    return personas;
                }
            }
            // Ejecutar la conexión y obtener la respuesta
            InputStreamReader isr = new InputStreamReader(conn.getInputStream());
        } catch (JAXBException ex) {
            Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE, null, ex);
        }
        

        return personas;
    }
    

    
}
