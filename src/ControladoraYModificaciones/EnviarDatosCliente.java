package ControladoraYModificaciones;

import datos.Persona;
import datos.Personas;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author Antonio
 */
public class EnviarDatosCliente {
 
    public static final byte DELETE = 0; // DELETE
    public static final byte POST = 1;   // INSERT
    public static final byte PUT = 2;    // UPDATE
    
    public static void main(String[] args) {
         
    }
    
    public void conexionServidor(Persona persona, byte accion){
            
            try {
                String strConnection = "http://localhost:8084/WebApplication1/servidorLocal";
                URL url = new URL(strConnection);
                URLConnection uc = url.openConnection();
                HttpURLConnection conn = (HttpURLConnection) uc;
                conn.setDoInput(true);
                conn.setDoOutput(true);
                conn.setRequestProperty("Content-type", "text/xml");

                // Se va a realizar una petición con el método POST
                conn.setRequestMethod("POST");

                Personas personas = new Personas();
                switch (accion) {
                    case DELETE:
                        // Escribir aquí las accciones para peticiones por DELETE
                        break;
                    case POST:
                        personas = añadirPersona(persona, personas);
                        break;
                    case PUT:
                        break;
                    default:
                        break;
                }     
                

                // Convertir objeto personas a XML y preparar para enviar al servidor.
                JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.marshal(personas, conn.getOutputStream());
                // Ejecutar la conexión y obtener la respuesta
                InputStreamReader isr = new InputStreamReader(conn.getInputStream());

            } catch (JAXBException ex) {
                Logger.getLogger(EnviarDatosCliente.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(EnviarDatosCliente.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    
        /**
         * Introduce una nueva persona en la base de datos.
         * @param persona Es el registro nuevo a introducir.
         * @param personas Son todas las personas que contendra la bases de datos.
         * @return 
         */
        public Personas añadirPersona(Persona persona, Personas personas){
            
            // Aunque sólo se va a enviar un único objeto, se utilizará un objeto
            //  de la clase Personas que almacenará dicho objeto. Se va a hacer
            //  así para usar siempre la clase Personas al generar los XML
            personas.getArrayListPersonas().add(persona);
            Logger.getLogger(EnviarDatosCliente.class.getName()).log(Level.INFO,
                    persona.getNombre());
            return personas;
        }
        
        public Personas modificarPersona(int indiceDeRegistro, Personas personas){
            
            
            return personas;
        }
 
}
