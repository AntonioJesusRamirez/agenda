package agenda;

import datos.Personas;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author Antonio
 */
public class Agenda extends Application {
    
    public static StackPane root = new StackPane();



    @Override
    public void start(Stage primaryStage) {
        
        
        Personas personas = ControladoraYModificaciones.ModificacionPersonaController.
                conexionServlet(null, ControladoraYModificaciones.
                ModificacionPersonaController.PETICION_GET); //PETICION_GET
        LeerPersona.datosPersona(personas);
        DisenoPersona disenoFichero = new DisenoPersona(ControladoraYModificaciones.
                ModificacionPersonaController.personas);

        root.getChildren().add(disenoFichero);        
        
        Scene scene = new Scene(root, 500, 350);
        
        primaryStage.setTitle("Registro de personas.");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    

 
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    public static void anadirBD(Personas personas) {

        LeerPersona.datosPersona();
        DisenoPersona disenoFichero = new DisenoPersona();
        root.getChildren().add(disenoFichero);
    }

}
