package agenda;

import ControladoraYModificaciones.ModificacionPersonaController;
import static ControladoraYModificaciones.ModificacionPersonaController.PETICION_DELETE;
import static ControladoraYModificaciones.ModificacionPersonaController.conexionServlet;
import static ControladoraYModificaciones.ModificacionPersonaController.persona;
import datos.Persona;
import datos.Personas;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

/**
 *
 * @author Antonio
 */

public class DisenoPersona extends VBox{
    
    private Logger logger = Logger.getLogger("");
//    private TableView tableView = new TableView();
    private TableView<Persona> tableView = new TableView<Persona>();
    public static boolean editarRegistro;
    Persona persona = new Persona();
    /**
     * Crea la tabla con los registros.
     */
    public DisenoPersona(){
        
//        ObservableList<Persona> observableList = 
//                FXCollections.observableArrayList(LeerPersona.getArrayList());


//                ModificacionPersonaController.initialize("", rb);
                ObservableList<Persona> observableList = 
                FXCollections.observableArrayList(ModificacionPersonaController.personas.getArrayListPersonas());
                
        tableView.setEditable(true);
        tableView.setItems(observableList);
        tableView = introducirResgistros();
        
        //Botones.
        Button nuevo = new Button("Nuevo");
        Button editar = new Button("Editar");
        Button suprimir = new Button("Suprimir");
        HBox hBoxMenu = new HBox();
        hBoxMenu.getChildren().addAll(nuevo, editar, suprimir);
        this.getChildren().addAll(hBoxMenu, tableView);        
        
        
        //Añadir un registro nuevo.
        nuevo.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent evt) {
                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource(
                        "/ControladoraYModificaciones/ModificacionPersona.fxml"));
                try {
                    Agenda.root.getChildren().add(fXMLLoader.load());
                    ModificacionPersonaController controller =
                            (ModificacionPersonaController)fXMLLoader.getController();
                    controller.setView(tableView);
                } catch (IOException ex) {
                    Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
        
        //Borrar un registro.
        suprimir.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent evt) {
//                getTableRow().getIndex();
                    int indiceDeRegistro = tableView.getSelectionModel().getSelectedIndex();
                    //Borramos el objeto obtenido de la fila.
                    observableList.remove(indiceDeRegistro);
                    Persona borrarPersona = tableView.getSelectionModel().getSelectedItem();
                    byte tipoPeticion;
                    tipoPeticion = PETICION_DELETE;
                    conexionServlet(borrarPersona, tipoPeticion);
            }

        });
        try{
        //Editar un registro.
        editar.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent evt) {
                editarRegistro = true;
                Persona personaFichero = (Persona) tableView.getSelectionModel().getSelectedItem();
                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource(
                        "/ControladoraYModificaciones/ModificacionPersona.fxml"));
                try {
                    Agenda.root.getChildren().add(fXMLLoader.load());
                    ModificacionPersonaController controller =
                            (ModificacionPersonaController)fXMLLoader.getController();
                    controller.setPersona(personaFichero);
                    controller.setView(tableView);
                } catch (IOException ex) {
                    Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
        }catch(NullPointerException ex){
            Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    /**
     * Introduce los registros en la tabla.
     * @return Devuelve un TableView con los registros.
     */
    public TableView introducirResgistros(){
        
        TableColumn nombreCol = new TableColumn("Nombre");
        nombreCol.setMinWidth(100);
        nombreCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("nombre"));
 
        TableColumn apellidosCol = new TableColumn("Apellidos");
        apellidosCol.setMinWidth(200);
        apellidosCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("apellidos"));

        TableColumn dniCol = new TableColumn("Dni");
        dniCol.setMinWidth(200);
        dniCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("dni"));
        
        TableColumn fechaNacCol = new TableColumn("FechaNac");
        fechaNacCol.setMinWidth(200);
        fechaNacCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("fechaNac"));
        
        TableColumn idCol = new TableColumn("ID");
        fechaNacCol.setMinWidth(200);
        fechaNacCol.setCellValueFactory(new PropertyValueFactory<Persona, String>("id"));
        
        tableView.getColumns().addAll(nombreCol, apellidosCol, dniCol, fechaNacCol,idCol);
        
        return tableView;
    }

    
    /**
     * Crea la tabla con los registros del servidor.
     */
    public DisenoPersona(Personas personas){
        
//        ObservableList<Persona> observableList = 
//                FXCollections.observableArrayList(LeerPersona.getArrayList());



                ObservableList<Persona> observableList = 
                FXCollections.observableArrayList(personas.getArrayListPersonas());
                
                
        tableView.setEditable(true);
        tableView.setItems(observableList);
        tableView = introducirResgistros();
        
        //Botones.
        Button nuevo = new Button("Nuevo");
        Button editar = new Button("Editar");
        Button suprimir = new Button("Suprimir");
        HBox hBoxMenu = new HBox();
        hBoxMenu.getChildren().addAll(nuevo, editar, suprimir);
        this.getChildren().addAll(hBoxMenu, tableView);        
        
        
        //Añadir un registro nuevo.
        nuevo.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent evt) {
                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource(
                        "/ControladoraYModificaciones/ModificacionPersona.fxml"));
                try {
                    Agenda.root.getChildren().add(fXMLLoader.load());
                    ModificacionPersonaController controller =
                            (ModificacionPersonaController)fXMLLoader.getController();
                    controller.setView(tableView);
                } catch (IOException ex) {
                    Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
        
        //Borrar un registro.
        suprimir.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent evt) {
//                getTableRow().getIndex();
                    int indiceDeRegistro = tableView.getSelectionModel().getSelectedIndex();
                    //Borramos el objeto obtenido de la fila.
                    observableList.remove(indiceDeRegistro);
                    Persona borrarPersona = tableView.getSelectionModel().getSelectedItem();
                    byte tipoPeticion;
                    tipoPeticion = PETICION_DELETE;
                    conexionServlet(borrarPersona, tipoPeticion);
            }

        });
        try{
        //Editar un registro.
        editar.setOnAction(new EventHandler<ActionEvent>(){
            @Override
            public void handle(ActionEvent evt) {
                editarRegistro = true;
                Persona personaFichero = (Persona) tableView.getSelectionModel().getSelectedItem();
                FXMLLoader fXMLLoader = new FXMLLoader(getClass().getResource(
                        "/ControladoraYModificaciones/ModificacionPersona.fxml"));
                try {
                    Agenda.root.getChildren().add(fXMLLoader.load());
                    ModificacionPersonaController controller =
                            (ModificacionPersonaController)fXMLLoader.getController();
                    controller.setPersona(personaFichero);
                    controller.setView(tableView);
                } catch (IOException ex) {
                    Logger.getLogger(Agenda.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        });
        }catch(NullPointerException ex){
            Logger.getLogger(ModificacionPersonaController.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
    
    
    
    
    
    
    
}
