package agenda;

import static ControladoraYModificaciones.ModificacionPersonaController.personas;
import datos.Persona;
import datos.Personas;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author Antonio
 */
public class LeerPersona {
    private static final Logger LOG = Logger.getLogger(LeerPersona.class.getName());
    private static ArrayList<Persona> arrayList = new ArrayList();

    public static ArrayList<Persona> getArrayList() {
        return arrayList;
    }
    
    /**
     * Lee los datos de un archivo en local .
     */
    public static void datosPersona(){
        SimpleDateFormat formatoParaFecha = new SimpleDateFormat("dd/MM/yy");
        String nombreFichero = "agenda.csv";
        //Declarar una variable BufferedReader
        BufferedReader br = null;
        try {
            //Crear un objeto BufferedReader al que se le pasa 
            //   un objeto FileReader con el nombre del fichero
            br = new BufferedReader(new FileReader(nombreFichero));
            //Leer la primera línea, guardando en un String
            String texto = br.readLine();
            //Repetir mientras no se llegue al final del fichero
            while(texto != null){
               
                String persona[] = texto.split(";");
                Date fechaNac = formatoParaFecha.parse(persona[9]);                
                Persona personaFichero = new Persona(persona[0], persona[1], 
                        persona[2], persona[3], persona[4], persona[5], 
                        persona[6], persona[7], persona[8], fechaNac);
                arrayList.add(personaFichero); 
                //Hacer lo que sea con la línea leída
                LOG.fine(personaFichero.getNombre() + "\t" + personaFichero.getApellidos()
                        + "\t" + personaFichero.getSexo() + "\t" + personaFichero.getDni()
                        + "\t" + personaFichero.getDireccion() + "\t" + personaFichero.getCP()
                        + "\t" + personaFichero.getCiudad() + "\t" + personaFichero.getTelefonoFijo()
                        + "\t" + personaFichero.getTelefonoMovil() + "\t" + personaFichero.getFechaNac());           
                //Leer la siguiente línea
                texto = br.readLine();
            }
            for(int i=0; i<arrayList.size(); i++){
                System.out.println(formatoParaFecha.format(arrayList.get(1).getFechaNac())); 
            }
        }
        catch (FileNotFoundException e) {
            System.out.println("Error: Fichero no encontrado");
            System.out.println(e.getMessage());
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }
        finally {
            try {
                if(br != null)
                    br.close();
            }
            catch (Exception e) {
                System.out.println("Error al cerrar el fichero");
                System.out.println(e.getMessage());
            }
        }
    }
    
    /**
     * Lee los datos del servidor para cargalos.
     */
    public static void datosPersona(Personas personas){

        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            for (Persona persona : personas.getArrayListPersonas()) {
//                String[] persona = null;

                
//                Date fechaNac = simpleDateFormat.parse(persona[9]);
//                Persona personaFichero = new Persona(persona[0], persona[1], 
//                        persona[2], persona[3], persona[4], persona[5], 
//                        persona[6], persona[7], persona[8], fechaNac);
                arrayList.add(persona); 
                //Hacer lo que sea con la línea leída
                LOG.fine(persona.getNombre() + "\t" + persona.getApellidos()
                        + "\t" + persona.getSexo() + "\t" + persona.getDni()
                        + "\t" + persona.getDireccion() + "\t" + persona.getCP()
                        + "\t" + persona.getCiudad() + "\t" + persona.getTelefonoFijo()
                        + "\t" + persona.getTelefonoMovil() + "\t" + persona.getFechaNac());           

            }
            for(int i=0; i<arrayList.size(); i++){
                System.out.println(simpleDateFormat.format(arrayList.get(1).getFechaNac())); 
            }
        }
        catch(Exception e) {
            System.out.println("Error de lectura del fichero");
            System.out.println(e.getMessage());
        }

    }
    
    
    
}
