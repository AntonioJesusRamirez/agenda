/**
 * Author: Antonio
 * Created: 28-abr-2016
 */

CREATE TABLE if not exists personas (
    ID INTEGER AUTO_INCREMENT,
    nombre VARCHAR(30),
    apellidos VARCHAR(50),
    sexo VARCHAR(20),
    dni VARCHAR(20),
    direccion VARCHAR(50),
    cP VARCHAR(20),
    ciudad VARCHAR(50),
    telefonoFijo VARCHAR(20),
    telefonoMovil VARCHAR(50),
    fechaNac DATE,
    PRIMARY KEY (ID)
);