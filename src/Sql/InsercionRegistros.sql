/**
 * Author:  Antonio
 * Created: 28-abr-2016
 */
INSERT INTO personas VALUES (null,'antonio','ramirez collado','masculino',
    '52645891-S','palomar, calle','23740','andujar','953525759','633123789','1974/04/12');
INSERT INTO personas VALUES (null,'alberto','rodriguez ferdandez','masculino',
    '53649273-R','ramon y cajal, calle','23700','linares','959587465','622131321','1989/01/01');
INSERT INTO personas VALUES (null,'jose','caballero navarro','masculino',
    '51234567-P','cornicabra, calle','11600','ubrique','998654123','654654654','1992/05/27');
INSERT INTO personas VALUES (null,'olga','saez alcantara','femenino',
    '59876543-Z','velillos, callejon','23850','bailen','999995558','666444999','2000/03/29');