package conversor;

import datos.Persona;
import datos.Personas;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 *
 * @author Antonio
 */
public class GenerarArrayList {
    
    private static final Logger LOG = Logger.getLogger(GenerarXML.class.getName());

    /**
     * Convierte el fichero perosonas.xml al ArrayList para mostrarlo por la salida estandar.
     */
    public static void generarArrayList(){
        
        try {
            BufferedReader br = new BufferedReader(new FileReader("personas.xml"));
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Personas personas = (Personas) jaxbUnmarshaller.unmarshal(br);
            for(Persona persona : personas.getArrayListPersonas()) {
                System.out.println("nombre: " + persona.getNombre());
                System.out.println("Apellidos: " + persona.getApellidos());
                System.out.println("DNI: " + persona.getDni());
                System.out.println();
            }
        } catch (JAXBException ex) {
            LOG.getLogger(GenerarArrayList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            LOG.getLogger(GenerarArrayList.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    
    
}
