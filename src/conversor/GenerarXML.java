package conversor;

import datos.Personas;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

/**
 *
 * @author Antonio
 */
public class GenerarXML {
    
    private static final Logger LOG = Logger.getLogger(GenerarXML.class.getName());

    /**
     * Convierte un arrayList a un fichero .XML en local.
     * @param arrayListPersonas que va a convertir en el fichero .XML
     */
    public static void generarXML(Personas arrayListPersonas){
        try {
            // Se indica el nombre de la clase que contiene la lista de objetos
            JAXBContext jaxbContext = JAXBContext.newInstance(Personas.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            // Indicar que se desea generar el xml con saltos de línea y tabuladores
            //  para facilitar su lectura
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            // Generar XML mostrándolo por la salida estándar
            jaxbMarshaller.marshal(arrayListPersonas, System.out);

            // Generar XML guardándolo en un archivo local
            BufferedWriter bw = new BufferedWriter(new FileWriter("personas.xml"));
            jaxbMarshaller.marshal(arrayListPersonas, bw);
        } catch (JAXBException ex) {
            LOG.getLogger(GenerarXML.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            LOG.getLogger(GenerarXML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
}
